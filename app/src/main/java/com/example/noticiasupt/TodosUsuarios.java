package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodosUsuarios extends AppCompatActivity {

    // AREGLOS QUE DONDE SE VAN A GUADAR LA RESPUESTA DEL JSON
    ArrayList<String> CatalogoUsuarios = new ArrayList<>();
    ArrayList<String> CatalogoId = new ArrayList<>();
    ArrayList<String> Catalogopassword = new ArrayList<>();

    //-- VARIABLES DE LOS ELEMENTOS QUE VOY A OCUPAR
    public ListView lvlistausuarios;
    public TextView tvusuario, tvid, tvpassword;

    //-- ARREGLOS QUE VAN A HEREDAR EL CONTENIDO DE LOS ARRAYLIST
    public String Detalles [];
    public String Password [];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todos_usuarios);

        tvusuario = (TextView) findViewById(R.id.tvdetalles);
        tvid = (TextView) findViewById(R.id.tvdetallesid);
        tvpassword = (TextView) findViewById(R.id.tvdetallespassword);
        lvlistausuarios = (ListView) findViewById(R.id.lvListaUsuarios);

        //--------------------------------------------- Codigo ListView

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.list_item_disenolu,CatalogoUsuarios);

        //ArrayAdapter<String>  adapter = new ArrayAdapter<String>(this,R.layout.list_item_disenolu, Usuarios);
        //lvlistausuarios.setAdapter(adapter);

        lvlistausuarios.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvusuario.setText("Usuario: " +lvlistausuarios.getItemAtPosition(position));
                tvid.setText("Id: "+ Detalles[position]);
                tvpassword.setText("Password: " + Password[position]);
            }
        });
        //cierre Button para la lista de usuarios

        //------------------------------------------------    Codigo Peticion   --------------------------------------------------


        ServicioPeticion service = Api.getApi(TodosUsuarios.this).create(ServicioPeticion.class);
        Call<Peticion_ListaUsuarios> iniciarCall = service.get_Usuarios();
        iniciarCall.enqueue(new Callback<Peticion_ListaUsuarios>() {
            @Override
            public void onResponse(Call<Peticion_ListaUsuarios> call, Response<Peticion_ListaUsuarios> response) {
                Peticion_ListaUsuarios peticion = response.body();

                if (peticion.estado.equals("true")){



                    List<ClaseUsuarios> usuarios = peticion.usuarios;

                    for (ClaseUsuarios mostrar : usuarios){
                        CatalogoUsuarios.add(mostrar.getUsername());
                    }
                    lvlistausuarios.setAdapter(arrayAdapter);
                    //----------------------------------------------
                    for (ClaseUsuarios mostrarid : usuarios){
                        CatalogoId.add(mostrarid.getId());
                    }

                    Detalles = CatalogoId.toArray(new String[CatalogoId.size()]);
                    //-----------------------------------------------
                    for (ClaseUsuarios mostrarpass : usuarios){
                        Catalogopassword.add(mostrarpass.getPassword());

                    }
                    Password = Catalogopassword.toArray(new String[Catalogopassword.size()]);



                }else{
                    Toast.makeText(TodosUsuarios.this,"Error 500",Toast.LENGTH_LONG).show();
                }

            }//OnResponse

            @Override
            public void onFailure(Call<Peticion_ListaUsuarios> call, Throwable t) {
                Toast.makeText(TodosUsuarios.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });
        //OnFail
        //-----------------------------------------------------------------------------------------------------------------------------




    }//Oncreate



}//Clase
