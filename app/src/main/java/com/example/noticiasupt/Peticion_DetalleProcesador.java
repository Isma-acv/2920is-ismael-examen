package com.example.noticiasupt;

import android.widget.ListView;

import java.util.List;

public class Peticion_DetalleProcesador {
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public List<Clase_DetallePro> getProcesador() {
        return procesador;
    }

    public void setProcesador(List<Clase_DetallePro> procesador) {
        this.procesador = procesador;
    }

    public String estado;
    public String detalle;
    public List<Clase_DetallePro> procesador;


}
