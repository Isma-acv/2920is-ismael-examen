package com.example.noticiasupt;

import java.util.ArrayList;

public class Peticion_ListaUsuarios {

    public String estado;
    public String detalle;
    public ArrayList<ClaseUsuarios> usuarios;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ClaseUsuarios> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<ClaseUsuarios> usuarios) {
        this.usuarios = usuarios;
    }




}
