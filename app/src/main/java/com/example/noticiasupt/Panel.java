package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Panel extends AppCompatActivity {

    //-- VARIABLES QUE VOY A OCUPAR
    public String Gnombre;
    ArrayList<String> catalogo = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);
        TextView Nombre = (TextView) findViewById(R.id.textViewName);
        //-- Codigo para guadar el nombre del usuario y mostrarlo
        //Obtener y y guadar el nombre de usuario
        String username = getIntent().getStringExtra("username");
        Gnombre = username;
        Guardarname();

        //Mostrar nombre

        SharedPreferences preferenciasnombre = getSharedPreferences("credencialesnombre", Context.MODE_PRIVATE);
        String gnombre = preferenciasnombre.getString("GNOMBRE", "");
        if (gnombre != ""){
            Nombre.setText(gnombre);
        }else {
            Nombre.setText(gnombre);
        }

        // TERMINO CODIGO PARA GUADRA EL NOMBRE


        Peticion();  //-- MANDO A LLAMAR EL METODO DE PETICION

    }//TERMINA Oncreate

    //-- METODO PARA CERRAR SESION
    public void CerrarSesion(View view){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = "";
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();

        Salir();

    }//TERMINO CerrarSesion

    //-- METODO PARA VOLVER AL LOGIN
    public void Salir(){

        Intent Salir = new Intent(Panel.this,MainActivity.class);
        startActivity(Salir);
    }// TERMINA Salir

    // METODO PARA GUARDAR EL NOMBRE EN UN ARCHIVO
    public void Guardarname(){
        SharedPreferences preferenciasnombre = getSharedPreferences("credencialesnombre", Context.MODE_PRIVATE);
        String gnombre = Gnombre;
        SharedPreferences.Editor editor = preferenciasnombre.edit();
        editor.putString("GNOMBRE", gnombre);
        editor.commit();

    }// TERMINO METEDO PARA GUADAR EL NOMBRE

    //-- METODO PARA EL BOTON DE IR A LISTA DE USUARIOS
    public void IrLitaUsuarios(View view){

        Intent IrUsuarios = new Intent(Panel.this,TodosUsuarios.class);
        startActivity(IrUsuarios);
    }//TERMINO IrListaUsuarios

    //--    Metodo para realizar la peticion de noticias
    public void Peticion(){


        ServicioPeticion service = Api.getApi(Panel.this).create(ServicioPeticion.class);
        Call<Peticion_Noticia> iniciarCall = service.get_Noticias();
        iniciarCall.enqueue(new Callback<Peticion_Noticia>() {
            @Override
            public void onResponse(Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                Peticion_Noticia peticion = response.body();
                if (peticion.estado.equals("true")){


                    List<ClaseDetalle> detalle = peticion.detalle;

                    for (ClaseDetalle mostrar : detalle){
                        catalogo.add(mostrar.getId());
                        catalogo.add(mostrar.getTitulo());
                        catalogo.add(mostrar.getDescripcion());

                    }



                    Toast.makeText(Panel.this,""+catalogo,Toast.LENGTH_LONG).show();

                }else{
                    Toast.makeText(Panel.this,"Error 500",Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Noticia> call, Throwable t) {
                Toast.makeText(Panel.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
            }
        });

    }  //Termina codigo para peticion


    //ir a discos

    public void IrPro(View view){
        Intent Irprocesadores = new Intent(Panel.this,Pro.class);
        startActivity(Irprocesadores);
    }
}//TERMINA Clase
