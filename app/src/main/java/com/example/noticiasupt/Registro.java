package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Registro extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


      Button  btnGuardar = (Button) findViewById(R.id.buttonRegistrame);
      btnGuardar.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              EditText nombreUsuario = (EditText) findViewById(R.id.editTextCorreo);
              EditText password1 = (EditText) findViewById(R.id.editTextContrasena);
              EditText password2 = (EditText) findViewById(R.id.editTextRepetirContra);

              String nombreUs = nombreUsuario.getText().toString();
              String pass1 = password1.getText().toString();
              String pass2 = password2.getText().toString();
              // -- Verficicar que no esten  nulos los campos


              if(TextUtils.isEmpty(nombreUs)){
                  nombreUsuario.setError("Agrege un nombre de Usuario");
                  nombreUsuario.requestFocus();
                  return;
              }
              if(TextUtils.isEmpty(pass1)){
                  password1.setError("Porfavor Ingrese una contraseña");
                  password1.requestFocus();
                  return;
              }
              if(TextUtils.isEmpty(pass2)){
                  password2.setError("Porfavor repita su contraseña");
                  password2.requestFocus();
                  return;
              }
              if(!pass1.equals(pass2)){
                  password1.setError("LAS CONTRASEÑAS NO COINCIDEN");
                  password1.requestFocus();
                  password2.setError("LAS CONTRASEÑAS NO COINCIDEN");
                  password2.requestFocus();
                  return;
              }


              // -- PETICION PARA REGISTRAR USUARIOS

              ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
              Call<Registro_Usuario> registrarCall = service.registrarUsuario(nombreUsuario.getText().toString(), password1.getText().toString());
              registrarCall.enqueue(new Callback<Registro_Usuario>() {
                  @Override
                  public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {

                      Registro_Usuario peticion = response.body();
                      if (response.body() == null){
                          Toast.makeText(Registro.this,"Ups! Error 500",Toast.LENGTH_SHORT).show();
                          return;
                      }
                      if (peticion.estado == "true"){
                          startActivity(new Intent(Registro.this,MainActivity.class));
                          Toast.makeText(Registro.this,"Felicidades!! Ya puedes Iniciar",Toast.LENGTH_LONG).show();
                      }else {
                          Toast.makeText(Registro.this, peticion.detalle,Toast.LENGTH_LONG).show();
                      }
                  }

                  @Override
                  public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                      Toast.makeText(Registro.this,"Ups! Algo salio mal",Toast.LENGTH_LONG).show();
                  }
              });


          }
      });

    }//ONCREATE





    //Metodo para Regresar a iniciar sesion
    public void PrefieroIS(View view){
        Intent IraIniciar = new Intent(Registro.this,MainActivity.class);
        startActivity(IraIniciar);
    }

}
